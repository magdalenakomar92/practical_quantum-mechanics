(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27991,        737]
NotebookOptionsPosition[     24647,        686]
NotebookOutlinePosition[     25002,        702]
CellTagsIndexPosition[     24959,        699]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Mechanika", " ", "macierzowa"}], "Title"], "  ", 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.7300142090670443`*^9, 3.730014265104718*^9}, {
   3.73001430435811*^9, 3.730014321320838*^9}, {3.73001506134825*^9, 
   3.730015064938594*^9}, {3.733036999900384*^9, 3.7330370011830482`*^9}, {
   3.7330372610494747`*^9, 3.733037295307342*^9}, 3.733037331475519*^9, {
   3.733379697461086*^9, 3.733379704398935*^9}, {3.7963590111527863`*^9, 
   3.796359031606551*^9}},ExpressionUUID->"d01db806-902a-4379-8c38-\
685481eb6749"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
   
   RowBox[{"$PrePrint", "=", "MatrixForm"}]}]}]], "Input",
 CellChangeTimes->{{3.7963595434149218`*^9, 3.796359554159768*^9}},
 CellLabel->
  "In[2099]:=",ExpressionUUID->"6f1f9e76-37c0-4f46-8011-faab197324f7"],

Cell[BoxData[
 TagBox["MatrixForm",
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.79635959583628*^9},
 CellLabel->
  "Out[2100]=",ExpressionUUID->"0c158117-b1f4-4d14-b4bb-6dfcfe1bb5e7"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", Cell["Funkcje ortonormalne :)", "Section",
   CellChangeTimes->{{3.7330374517597923`*^9, 3.733037461880712*^9}, {
    3.733037497789156*^9, 3.733037502111595*^9}},ExpressionUUID->
   "dd59143d-9aea-4a02-a4a8-ea2f14c14e7d"]}]], "Input",
 CellChangeTimes->{{3.796359572384039*^9, 
  3.796359587860304*^9}},ExpressionUUID->"8bd3c99c-5d2f-42e7-8316-\
5f0176cc4e02"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"nmax", "=", "8"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"u1", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Exp", "[", 
        RowBox[{
         RowBox[{"-", 
          RowBox[{"x", "^", "2"}]}], "/", "2"}], "]"}], " ", 
       RowBox[{"HermiteH", "[", 
        RowBox[{
         RowBox[{"n", "-", "1"}], ",", "x"}], "]"}]}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "nmax"}], "}"}]}], "]"}], " ", 
    RowBox[{"(*", " ", 
     RowBox[{
      RowBox[{
      "funkcje", " ", "oscylatora", " ", "z", " ", "Hamiltonianem", " ", 
       "\[IndentingNewLine]", "H"}], "=", "  ", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], " ", 
        SuperscriptBox[
         RowBox[{"(", 
          FractionBox["\[DifferentialD]", 
           RowBox[{"\[DifferentialD]", "x"}]], ")"}], "2"]}], "  ", "+", " ", 
       
       RowBox[{
        FractionBox["1", "2"], " ", 
        SuperscriptBox["x", "2"], "     ", "\[IndentingNewLine]", 
        RowBox[{"Sprawd\:017a", "!"}]}]}]}], " ", "*)"}]}]}]}]], "Input",
 CellChangeTimes->{{3.79635960448878*^9, 3.7963596299769173`*^9}, {
  3.8129606895812407`*^9, 3.812960707299117*^9}, {3.812960746661948*^9, 
  3.812960991248598*^9}},ExpressionUUID->"17e6d6ce-6c3d-45c4-b643-\
fa9f8a131d24"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"funkcje", " ", "u1", " ", "s\:0105", " ", "ortogonalne"}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"norm", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Chop", "[", 
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"u1", "[", 
           RowBox[{"[", "n", "]"}], "]"}], " ", 
          RowBox[{"u1", "[", 
           RowBox[{"[", "m", "]"}], "]"}]}], ",", " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", " ", 
           RowBox[{"-", "Infinity"}], ",", " ", "Infinity"}], "}"}]}], "]"}], 
       "]"}], ",", " ", "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "nmax"}], "}"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"m", ",", "nmax"}], "}"}]}], "]"}]}], 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
      RowBox[{
      "Macierz", " ", "iloczyn\[OAcute]w", " ", "skalarnych", "  ", 
       "funkcji"}], " ", "-", " ", 
      RowBox[{
      "jest", " ", "diagonalna", " ", "\[IndentingNewLine]", "to", " ", 
       "daje", " ", "normy"}]}], ",", " ", 
     RowBox[{
      RowBox[{"a", " ", "dla", " ", "n"}], "\[NotEqual]", 
      RowBox[{"m", " ", "-", " ", 
       RowBox[{"zero", "\[IndentingNewLine]", "Chop"}], " ", "-", " ", 
       RowBox[{"obciecie", " ", "smieci"}]}]}]}], "*)"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.7963596446060257`*^9, 3.7963597112041388`*^9}, {
  3.8129603343250647`*^9, 3.812960351472303*^9}, {3.8129604374973717`*^9, 
  3.812960458590313*^9}, {3.8129610150309057`*^9, 
  3.812961015639213*^9}},ExpressionUUID->"0dc4a3f9-d3fa-4307-bd5b-\
ce2d15573aa1"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"funkcje", " ", "u", " ", "s\:0105", " ", "ortonormalne"}], " ", 
    "-", " ", 
    RowBox[{"baza", " ", "w", " ", "przestrzeni"}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"u", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"u1", "[", 
        RowBox[{"[", "n", "]"}], "]"}], "/", 
       RowBox[{"Sqrt", "[", 
        RowBox[{"norm", "[", 
         RowBox[{"[", 
          RowBox[{"n", ",", "n"}], "]"}], "]"}], "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "nmax"}], "}"}]}], "]"}]}], "  ", 
   "\[IndentingNewLine]", 
   RowBox[{"norm1", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Chop", "[", 
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"u", "[", 
           RowBox[{"[", "n", "]"}], "]"}], " ", 
          RowBox[{"u", "[", 
           RowBox[{"[", "m", "]"}], "]"}]}], ",", " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", " ", 
           RowBox[{"-", "Infinity"}], ",", " ", "Infinity"}], "}"}]}], "]"}], 
       "]"}], ",", " ", "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "nmax"}], "}"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"m", ",", "nmax"}], "}"}]}], "]"}], 
    RowBox[{"(*", 
     RowBox[{"sa", " ", "ortonormalne"}], "*)"}]}]}]}]], "Input",
 CellChangeTimes->{
  3.796359768206674*^9, {3.796359819545759*^9, 3.796359853601054*^9}, {
   3.796360364660956*^9, 3.796360365058346*^9}, {3.7963604009394197`*^9, 
   3.796360412042321*^9}},
 CellLabel->
  "In[2104]:=",ExpressionUUID->"f4727364-dda1-4aee-ad09-f6246e92055e"],

Cell[TextData[{
 "\nRozwini\:0119cie funkcji w bazie funkcji ortonormalnych\nKa\:017cd\:0105 \
funkcj\:0119 ca\[LSlash]kowan\:0105 z kwadratem mog\:0119 rozwin\:0105\
\[CAcute]\nf(x)=",
 Cell[BoxData[
  FormBox[
   RowBox[{
    UnderoverscriptBox["\[Sum]", 
     RowBox[{"n", "=", "0"}], "\[Infinity]"], 
    SubscriptBox["c", "n"]}], TraditionalForm]],ExpressionUUID->
  "e3998265-1210-4419-bdcd-31e257827c63"],
 Cell[BoxData[
  FormBox[
   SubscriptBox["u", "n"], TraditionalForm]],ExpressionUUID->
  "c994f563-2b0b-444b-8515-8411a7e2f346"],
 "(x)\nczyli funkcj\:0119 mo\:017cna opisa\[CAcute] przez (niesko\:0144czony) \
wektor ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["c", "n"], TraditionalForm]],ExpressionUUID->
  "05b73265-12cf-4e43-9044-b76941cb754e"],
 "\nWsp\[OAcute]\[LSlash]czynniki ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["c", "n"], TraditionalForm]],ExpressionUUID->
  "15c91afa-3b57-4db8-905f-301a39e5c865"],
 "szybko malej\:0105 z n, mo\:017cna zachowa\[CAcute] tylko par\:0119 \
pierwszych\nPozwala to operowa\[CAcute] sko\:0144czonymi wektorami zamiast \
funkcji"
}], "Text",
 CellChangeTimes->{
  3.7963604663090343`*^9, {3.796360576569909*^9, 3.796360965450156*^9}, {
   3.796361004258238*^9, 3.796361032042117*^9}, 
   3.8129600547287683`*^9},ExpressionUUID->"b86b9099-5661-4818-9254-\
97532b54370f"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"badam", " ", "przyk\[LSlash]adow\:0105", " ", "funkcje"}], " ", 
   "*)"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{
  3.796361117795609*^9, {3.7963611919705267`*^9, 
   3.796361217926395*^9}},ExpressionUUID->"7334c097-5a0d-4301-bde1-\
487993b30745"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"fun", "=", 
   RowBox[{"1.0", " ", "/", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"x", "-", "1"}], ")"}], "^", "2"}]}], ")"}], "^", "2"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"xmax", "=", "5"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"pl1", "=", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"fun", ",", " ", "0"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"-", "xmax"}], ",", " ", "xmax"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"PlotRange", "\[Rule]", "All"}], ",", "\[IndentingNewLine]", 
    RowBox[{"PlotStyle", "\[Rule]", 
     RowBox[{"{", 
      RowBox[{"Red", ",", " ", "Black"}], "}"}]}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.796361259104491*^9, 3.7963612776886883`*^9}, {
  3.7963613256334143`*^9, 3.796361330973831*^9}},
 CellLabel->
  "In[2109]:=",ExpressionUUID->"fe88b6b0-c265-4fb7-8526-d3f87c37363a"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
   "Print", "[", "\"\<Wsp\[OAcute]\[LSlash]czynniki rozwini\:0119cia\>\"", 
    "]"}], "\[IndentingNewLine]", 
   RowBox[{"c", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Chop", "[", 
       RowBox[{"NIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"u", "[", 
           RowBox[{"[", "n", "]"}], "]"}], " ", "fun"}], ",", " ", 
         RowBox[{"{", 
          RowBox[{"x", ",", " ", 
           RowBox[{"-", "Infinity"}], ",", " ", "Infinity"}], "}"}]}], "]"}], 
       "]"}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "nmax"}], "}"}]}], "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.7963613540636587`*^9, 3.796361380320094*^9}},
 CellLabel->
  "In[2112]:=",ExpressionUUID->"16443bbf-ea3f-4245-91a7-bd8459be0d60"],

Cell[BoxData["\<\"Wsp\[OAcute]\[LSlash]czynniki rozwini\:0119cia\"\>"], \
"Print",
 CellChangeTimes->{3.796361384656766*^9},
 CellLabel->
  "During evaluation of \
In[2112]:=",ExpressionUUID->"883eaf8e-be91-44db-930e-d29206292eb0"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"fun1", " ", "-", " ", 
    RowBox[{
    "przrbli\:017cenie", " ", "szeregiem", " ", "funkcji", " ", 
     "ortogonalnych"}]}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"fun1", "=", 
    RowBox[{"Sum", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"c", "[", 
        RowBox[{"[", "n", "]"}], "]"}], " ", 
       RowBox[{"u", "[", 
        RowBox[{"[", "n", "]"}], "]"}]}], ",", " ", 
      RowBox[{"{", 
       RowBox[{"n", ",", " ", "nmax"}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{
  3.796361469751329*^9, {3.7963615048510637`*^9, 3.796361578429419*^9}},
 CellLabel->
  "In[2115]:=",ExpressionUUID->"3ff97492-13d5-4a62-94d6-78d2864cba1f"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"tytul", "=", 
   RowBox[{"\"\<Funkcja \>\"", "<>", 
    RowBox[{"ToString", "[", 
     RowBox[{"fun", ",", 
      RowBox[{"FormatType", "\[Rule]", "TraditionalForm"}]}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"fun", ",", "fun1", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{"-", "xmax"}], ",", "xmax"}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{"PlotLabel", "\[Rule]", "tytul"}], ",", "\[IndentingNewLine]", 
   RowBox[{"PlotRange", "\[Rule]", "All"}], ",", "\[IndentingNewLine]", 
   RowBox[{"PlotStyle", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"Red", ",", "Blue", ",", "Black"}], "}"}]}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"PlotLegends", "\[Rule]", 
    RowBox[{"SwatchLegend", "[", 
     RowBox[{"{", 
      RowBox[{"\"\<Funkcja\>\"", ",", "\"\<Przybli\:017cenie\>\""}], "}"}], 
     "]"}]}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.796361607713706*^9, 3.796361702324156*^9}, {
   3.7963617342849483`*^9, 3.796361779311912*^9}, 3.7963622413485518`*^9, {
   3.796364938506378*^9, 3.79636502501556*^9}, {3.796365253021948*^9, 
   3.796365256994277*^9}, {3.796365333346737*^9, 3.796365387304914*^9}, {
   3.796365421931458*^9, 3.796365444649111*^9}, {3.796365508948476*^9, 
   3.796365509511725*^9}, {3.796365765338358*^9, 3.796365807030447*^9}, {
   3.7963658818759613`*^9, 3.796365887715563*^9}, {3.796365918232223*^9, 
   3.7963659423994923`*^9}, {3.796366035364779*^9, 3.796366041882556*^9}},
 CellLabel->
  "In[2133]:=",ExpressionUUID->"78b5c8fb-58c9-404d-a734-766515b2d7d8"],

Cell[CellGroupData[{

Cell["\<\
Zadanie
\
\>", "Section",
 CellChangeTimes->{
  3.796369613982606*^9, {3.796371412588266*^9, 
   3.796371428607977*^9}},ExpressionUUID->"b27d8fc0-6455-4e65-a186-\
eea8490a8a59"],

Cell["Zr\[OAcute]b to samo dla funkcji:", "Text",
 CellChangeTimes->{{3.796371447441195*^9, 
  3.796371464551626*^9}},ExpressionUUID->"e16adda2-573b-4395-b9c0-\
b1fc89b39149"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"funkcja", " ", "przyk\[LSlash]adowa"}], ",", " ", 
    RowBox[{
    "wolno", " ", "zanikaj\:0105ca", " ", "w", " ", 
     "niesko\:0144czono\:015bci"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"fun", "=", 
    RowBox[{"1.0", " ", 
     RowBox[{"x", "/", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"x", "-", "1"}], ")"}], "^", "2"}]}], ")"}]}]}]}], ";", 
   RowBox[{"(*", 
    RowBox[{"na", " ", "granicy", " ", "ca\[LSlash]kowalno\:015bci"}], 
    "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.796371496042378*^9, 
  3.796371497320404*^9}},ExpressionUUID->"35d4b995-a31c-420b-b21a-\
ffc3f332cff8"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", Cell["Mechanika macierzowa", "Section",
   CellChangeTimes->{{3.7336406704848804`*^9, 3.7336407013993287`*^9}, {
    3.733640748516021*^9, 3.7336407623449*^9}},ExpressionUUID->
   "9c2a9279-a46f-4dcc-bedd-b500622eb1b7"]}]], "Input",
 CellChangeTimes->{{3.796371548906385*^9, 
  3.7963715501177998`*^9}},ExpressionUUID->"9b208c4e-ba89-495a-9dc1-\
15d01158acec"],

Cell[TextData[{
 "\nSkoro funkcje mo\:017cemy przybli\:017ca\[CAcute] wektorami, to operatory \
liniowe to s\:0105 macierze.\nMacierze te s\:0105 formalnie \
niesko\:0144czone, ale mo\:017cna je przybli\:017cy\[CAcute] macierzami sko\
\:0144czonymi.\nMo\:017cna pokaza\[CAcute], \:017ce operatory a  anihilaci a \
i kreacji ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["a", 
    RowBox[{"+", " "}]], TraditionalForm]],ExpressionUUID->
  "68ef337b-de25-460d-a2cd-c6ecc350d3e5"],
 "s\:0105 proste (patrz ni\:017cej).\noperatory te spe\[LSlash]niaj\:0105 \
relacj\:0119 komutacji [a,",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["a", 
    RowBox[{"+", " "}]], TraditionalForm]],ExpressionUUID->
  "91cabe04-5f91-40ba-ba50-3cec739d20b5"],
 "]= a",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SuperscriptBox["a", 
      RowBox[{"+", " "}]], "-", " ", 
     RowBox[{
      SuperscriptBox["a", 
       RowBox[{"+", " "}]], " ", "a"}]}], " ", "="}], TraditionalForm]],
  ExpressionUUID->"21be1577-77fa-4773-8cfe-d9ae6252e2f6"],
 " 1 (macierz jednostkowa).\nDla sko\:0144czonego przybli\:017cenia tak nie \
mo\:017ce byc, ale b\[LSlash]\:0105d jest na dalekim miejscu.\nW Mathematica \
mno\:017cenie macierzy robi si\:0119 operatorem \[OpenCurlyDoubleQuote].\
\[CloseCurlyDoubleQuote] (kropka)\nJe\:015bli mata,matb,matc s\:0105 \
macierzami to mo\:017cna np.   napisa\[CAcute] matc=mata.matb"
}], "Text",
 CellChangeTimes->{{3.796371591643732*^9, 3.796371764956072*^9}, {
   3.796371806524701*^9, 3.796371855203895*^9}, {3.796371896827744*^9, 
   3.796371934653111*^9}, {3.796372048865983*^9, 3.796372207134797*^9}, {
   3.796372259664707*^9, 3.7963724187399883`*^9}, 3.796372451470455*^9, 
   3.79637248731153*^9, {3.796373741312134*^9, 3.796373864765635*^9}, {
   3.7963915453682747`*^9, 
   3.79639156780622*^9}},ExpressionUUID->"486229b3-d25f-4064-809a-\
ba2fb3816bcd"],

Cell[BoxData[{
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"nmax", " ", "=", " ", "8"}], ";", " ", 
  RowBox[{"(*", 
   RowBox[{"pracujemy", " ", "na", " ", "macierzach", " ", "8", "x8"}], 
   "*)"}], "\[IndentingNewLine]"}]}], "Input",
 CellChangeTimes->{3.796373664564805*^9, 3.796373695836182*^9},
 CellLabel->
  "In[2135]:=",ExpressionUUID->"d9526832-6eb9-4870-b973-083cd5dbad77"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Operator", " ", "anihilacji"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"a", " ", "=", " ", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Which", "[", 
       RowBox[{
        RowBox[{"m", " ", "==", " ", 
         RowBox[{"n", "+", "1"}]}], ",", 
        RowBox[{"Sqrt", "[", "n", "]"}], ",", "True", ",", "0"}], "]"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"n", ",", "nmax"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"m", ",", "nmax"}], "}"}]}], "]"}]}], " ", 
   RowBox[{"(*", 
    RowBox[{"Macierz", " ", "niesymetryczna"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"Operator", " ", "kreacji"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"ad", " ", "=", " ", 
    RowBox[{"Transpose", "[", "a", "]"}], " "}]}]}]], "Input",
 CellChangeTimes->{
  3.796373874064061*^9, {3.796373907776988*^9, 3.796373921700791*^9}},
 CellLabel->
  "In[2137]:=",ExpressionUUID->"f521c5fc-ff39-4fac-81be-9e87a57d357f"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "tu", " ", "sprawd\:017a", " ", "relacj\:0119", " ", "komutacji", " ", 
     "\[IndentingNewLine]", "jest", " ", "on", " ", "poprawny", " ", "za", 
     " ", "wyj\:0105tkiem", " ", "nojwiekszego", " ", 
     "wsp\[OAcute]\[LSlash]czynnika"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"ale", " ", "to", " ", "nie", " ", "problem"}], "-", " ", 
     RowBox[{
     "nasze", " ", "wektory", " ", "s\:0105", " ", "tu", " ", "bardzo", " ", 
      "ma\[LSlash]e"}]}]}], "*)"}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.7963739468444967`*^9, 3.7963739751237907`*^9}, {
  3.812960559493843*^9, 
  3.812960651619186*^9}},ExpressionUUID->"70ef4333-70a3-4dc9-bf63-\
d334d03483e0"],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
  "wprowadzamy", " ", "operator", " ", "po\[LSlash]o\:017cenia", " ", "opx", 
   " ", "i", " ", "p\:0119du", " ", "opp"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.796374146075779*^9, 3.796374251704616*^9}, {
  3.796374291826022*^9, 
  3.796374313727635*^9}},ExpressionUUID->"fd6c66b0-eabd-44ce-ab53-\
585b810521b1"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", "    ", 
   RowBox[{"Operator", " ", "po\[LSlash]o\:017cenia", " ", "x"}], "    ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{"opx", " ", "=", " ", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"a", "+", "ad"}], ")"}], "/", 
    RowBox[{"Sqrt", "[", "2", "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.796390002368816*^9, 
  3.7963900221993027`*^9}},ExpressionUUID->"91e80af1-6033-4b2b-add4-\
945e6df47d24"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"operator", " ", "p\:0119du", " ", "p"}], "   ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"opp", " ", "=", " ", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"a", "-", "ad"}], ")"}], "/", 
    RowBox[{"(", 
     RowBox[{"I", " ", 
      RowBox[{"Sqrt", "[", "2", "]"}]}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.796374260675496*^9, 3.7963742809626093`*^9}, {
  3.7963900283061132`*^9, 
  3.796390074072757*^9}},ExpressionUUID->"74bbedb8-83ed-4a31-a174-\
69c2f0fbc0ff"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"tu", " ", "sprawd\:017a"}], ",", " ", 
    RowBox[{
     RowBox[{"\:017ce", " ", "[", 
      RowBox[{"x", ",", "p"}], "]"}], "=", "\[ImaginaryI]", 
     " "}]}]}]}]], "Input",
 CellChangeTimes->{{3.79639011271192*^9, 
  3.796390168826623*^9}},ExpressionUUID->"56a91f08-d39d-4148-9b7a-\
09fa1edc84e7"],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{
  3.796390184934862*^9},ExpressionUUID->"b52fd119-9ef1-43a7-b523-\
4cbaa1806e11"],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
   RowBox[{
    RowBox[{
    "tu", " ", "utworz", " ", "operator", " ", "dla", " ", "oscylatora", " ", 
     "harmonicznego", " ", "z", " ", "hamiltonianem", "\[IndentingNewLine]", 
     "ham"}], "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"p", "^", "2"}], "/", "2"}], "+", 
     RowBox[{
      RowBox[{
       RowBox[{"x", "^", "2"}], "/", "2"}], "\[IndentingNewLine]", 
      "Zobacz"}]}]}], ",", " ", 
   RowBox[{"\:017ce", " ", "operator", " ", "jest", " ", "diagonalny"}], ",", 
   " ", 
   RowBox[{
   "na", " ", "diagonali", " ", "ma", " ", "warto\:015bci", " ", 
    "w\[LSlash]asne", " ", 
    RowBox[{"(", "energie", ")"}]}]}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.79637500664331*^9, 3.79637514285709*^9}, {
  3.7963897991098013`*^9, 
  3.796389814347101*^9}},ExpressionUUID->"425a4f6f-deb7-4a7d-8f42-\
24b05da62185"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", Cell["Oscylator kwartyczny", "Section",
   CellChangeTimes->{{3.7336431194077487`*^9, 3.733643123245099*^9}},
   ExpressionUUID->"cb04e8c7-b6b3-43ed-afb6-3c4c679d39c2"]}]], "Input",
 CellChangeTimes->{{3.7963902646193*^9, 
  3.796390295886969*^9}},ExpressionUUID->"4f92358c-7b78-4a9e-8ca5-\
753a0b6f5d87"],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "oscylator", " ", "kwartyczny", " ", "ma", " ", "hamiltonian", 
     "\[IndentingNewLine]", "h4"}], " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"p", "^", "2"}], "/", "2"}], "+", 
     RowBox[{"x", "^", "4"}]}]}], "   ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"h4", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"opp", ".", "opp"}], "/", "2"}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"opx", ".", "opx"}], ")"}], ".", 
     RowBox[{"(", 
      RowBox[{"opx", ".", "opx"}], ")"}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.796390351884514*^9, 
  3.796390534473214*^9}},ExpressionUUID->"6b92eacb-e6ab-41f9-9574-\
5dfeaf2d0b49"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "energie", " ", "znajdujemy", " ", "za", " ", "pomoc\:0105", " ", 
    "funkcji", " ", "szukaj\:0105cej", " ", "warto\:015bci", " ", 
    "w\[LSlash]asnych"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"energie4", "=", 
   RowBox[{"Eigenvalues", "[", 
    RowBox[{"1.0", "h4"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7963905434063807`*^9, 3.796390643575493*^9}, 
   3.796390785682233*^9},ExpressionUUID->"991e3728-cb76-4ae9-ba2d-\
3ade86b00894"],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
   RowBox[{
   "Nie", " ", "wszystkie", " ", "energie", " ", "s\:0105", " ", "dobre", 
    "\[IndentingNewLine]", "Aby", " ", "zobaczy\[CAcute]"}], ",", " ", 
   RowBox[{"kt\[OAcute]rym", " ", "mo\:017cna", " ", "ufa\[CAcute]"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
    "zanotuj", " ", "energie4", " ", "i", " ", "powt\[OAcute]rz", " ", 
     "rachunek", " ", "z", " ", "nmax"}], "=", 
    RowBox[{"16", " ", "-", " ", 
     RowBox[{
     "to", " ", "da", " ", "energie16", "\[IndentingNewLine]", 
      "zobaczysz"}]}]}], ",", " ", 
   RowBox[{
   "\:017ce", " ", "tylko", " ", "pare", " ", "najni\:017cszych", " ", 
    "si\:0119", " ", "ma\[LSlash]o", " ", "zmieni", "\[IndentingNewLine]", 
    "r\[OAcute]\:017cnica", " ", "pomiedzy", " ", "energie16", " ", "i", " ", 
    "energie8", " ", "daj\:0119", " ", "b\[LSlash]\:0105d", " ", 
    "oblicze\:0144"}]}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.796390707840382*^9, 3.7963907577181797`*^9}, {
  3.796390802454246*^9, 3.796390922387949*^9}, {3.796390980251848*^9, 
  3.7963910401638813`*^9}, {3.7963910771226387`*^9, 
  3.796391141609151*^9}},ExpressionUUID->"5a24f668-e4a9-4628-a4dc-\
40e36d2710f3"]
}, Open  ]]
},
WindowSize->{808, 855},
WindowMargins->{{295, Automatic}, {Automatic, 19}},
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 581, 11, 74, "Input",ExpressionUUID->"d01db806-902a-4379-8c38-685481eb6749"],
Cell[CellGroupData[{
Cell[1164, 35, 351, 8, 73, "Input",ExpressionUUID->"6f1f9e76-37c0-4f46-8011-faab197324f7"],
Cell[1518, 45, 224, 6, 34, "Output",ExpressionUUID->"0c158117-b1f4-4d14-b4bb-6dfcfe1bb5e7"]
}, Open  ]],
Cell[1757, 54, 407, 7, 80, "Input",ExpressionUUID->"8bd3c99c-5d2f-42e7-8316-5f0176cc4e02"],
Cell[2167, 63, 1403, 39, 142, "Input",ExpressionUUID->"17e6d6ce-6c3d-45c4-b643-fa9f8a131d24"],
Cell[3573, 104, 1716, 44, 157, "Input",ExpressionUUID->"0dc4a3f9-d3fa-4307-bd5b-ce2d15573aa1"],
Cell[5292, 150, 1710, 48, 115, "Input",ExpressionUUID->"f4727364-dda1-4aee-ad09-f6246e92055e"],
Cell[7005, 200, 1330, 34, 173, "Text",ExpressionUUID->"b86b9099-5661-4818-9254-97532b54370f"],
Cell[8338, 236, 345, 8, 73, "Input",ExpressionUUID->"7334c097-5a0d-4301-bde1-487993b30745"],
Cell[8686, 246, 1023, 29, 115, "Input",ExpressionUUID->"fe88b6b0-c265-4fb7-8526-d3f87c37363a"],
Cell[CellGroupData[{
Cell[9734, 279, 843, 23, 73, "Input",ExpressionUUID->"16443bbf-ea3f-4245-91a7-bd8459be0d60"],
Cell[10580, 304, 231, 5, 24, "Print",ExpressionUUID->"883eaf8e-be91-44db-930e-d29206292eb0"]
}, Open  ]],
Cell[10826, 312, 781, 22, 94, "Input",ExpressionUUID->"3ff97492-13d5-4a62-94d6-78d2864cba1f"],
Cell[11610, 336, 1643, 36, 136, "Input",ExpressionUUID->"78b5c8fb-58c9-404d-a734-766515b2d7d8"],
Cell[CellGroupData[{
Cell[13278, 376, 187, 7, 105, "Section",ExpressionUUID->"b27d8fc0-6455-4e65-a186-eea8490a8a59"],
Cell[13468, 385, 175, 3, 35, "Text",ExpressionUUID->"e16adda2-573b-4395-b9c0-b1fc89b39149"],
Cell[13646, 390, 762, 22, 94, "Input",ExpressionUUID->"35d4b995-a31c-420b-b21a-ffc3f332cff8"],
Cell[14411, 414, 406, 7, 80, "Input",ExpressionUUID->"9b208c4e-ba89-495a-9dc1-15d01158acec"],
Cell[14820, 423, 1880, 42, 196, "Text",ExpressionUUID->"486229b3-d25f-4064-809a-ba2fb3816bcd"],
Cell[16703, 467, 437, 9, 73, "Input",ExpressionUUID->"d9526832-6eb9-4870-b973-083cd5dbad77"],
Cell[17143, 478, 1019, 27, 115, "Input",ExpressionUUID->"f521c5fc-ff39-4fac-81be-9e87a57d357f"],
Cell[18165, 507, 796, 17, 115, "Input",ExpressionUUID->"70ef4333-70a3-4dc9-bf63-d334d03483e0"],
Cell[18964, 526, 366, 8, 30, "Input",ExpressionUUID->"fd6c66b0-eabd-44ce-ab53-585b810521b1"],
Cell[19333, 536, 446, 12, 52, "Input",ExpressionUUID->"91e80af1-6033-4b2b-add4-945e6df47d24"],
Cell[19782, 550, 555, 15, 73, "Input",ExpressionUUID->"74bbedb8-83ed-4a31-a174-69c2f0fbc0ff"],
Cell[20340, 567, 394, 11, 52, "Input",ExpressionUUID->"56a91f08-d39d-4148-9b7a-09fa1edc84e7"],
Cell[20737, 580, 147, 3, 52, "Input",ExpressionUUID->"b52fd119-9ef1-43a7-b523-4cbaa1806e11"],
Cell[20887, 585, 889, 24, 94, "Input",ExpressionUUID->"425a4f6f-deb7-4a7d-8f42-24b05da62185"],
Cell[21779, 611, 351, 6, 80, "Input",ExpressionUUID->"4f92358c-7b78-4a9e-8ca5-753a0b6f5d87"],
Cell[22133, 619, 746, 22, 94, "Input",ExpressionUUID->"6b92eacb-e6ab-41f9-9574-5dfeaf2d0b49"],
Cell[22882, 643, 516, 12, 52, "Input",ExpressionUUID->"991e3728-cb76-4ae9-ba2d-3ade86b00894"],
Cell[23401, 657, 1230, 26, 115, "Input",ExpressionUUID->"5a24f668-e4a9-4628-a4dc-40e36d2710f3"]
}, Open  ]]
}
]
*)

