(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     31415,        780]
NotebookOptionsPosition[     27766,        720]
NotebookOutlinePosition[     28141,        737]
CellTagsIndexPosition[     28098,        734]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[" Oscylator harmoniczny 1", "Title",
 CellChangeTimes->{{3.7288102551284313`*^9, 3.728810257166582*^9}, {
  3.7324334162311516`*^9, 3.7324334402543855`*^9}, {3.795156326964199*^9, 
  3.7951563294307737`*^9}},ExpressionUUID->"17a1f910-0a7c-4b08-9383-\
7f40d283aabf"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{"$PrePrint", "=", "MatrixForm"}]}], "Input",
 CellChangeTimes->{{3.728810396229485*^9, 3.72881047947025*^9}, {
   3.7288106012542*^9, 3.728810618810606*^9}, {3.7288106612822723`*^9, 
   3.7288106922349997`*^9}, {3.7324334049180636`*^9, 3.732433407386944*^9}, 
   3.799610640755702*^9},
 CellLabel->"In[61]:=",ExpressionUUID->"4ef42661-f4d0-4d12-81d6-7a0094905056"],

Cell[BoxData[
 TagBox["MatrixForm",
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.732433411027716*^9, 3.733379126780671*^9, 
  3.733379255356594*^9, 3.73337944708519*^9, 3.795156340128016*^9, 
  3.795156948966813*^9, 3.7996106556936216`*^9, 3.8171015282500143`*^9, 
  3.817107167824061*^9},
 CellLabel->"Out[62]=",ExpressionUUID->"c3e2c797-92bf-4df8-a9c1-84e62df0626e"]
}, Open  ]],

Cell[TextData[{
 "\nWe study one dimensional harmonic oscillator    (\[HBar] = 1, m=1, k=1)\n\
classicaly we have the Hamiltonian: ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["H", "classical"], "=", 
    RowBox[{
     FractionBox[
      SuperscriptBox["p", "2"], "2"], "+", 
     FractionBox[
      SuperscriptBox["x", "2"], "2"]}]}], TraditionalForm]],ExpressionUUID->
  "c9ac24b2-6742-48ee-a160-837029ca04c3"],
 "\nQuantum Hamiltonian: ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    OverscriptBox["H", "^"], "=", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       RowBox[{"(", 
        FractionBox["1", "2"], ")"}]}], 
      FractionBox[
       SuperscriptBox["d", "2"], 
       SuperscriptBox["dx", "2"]]}], "+", 
     FractionBox[
      SuperscriptBox["x", "2"], "2"]}]}], TraditionalForm]],ExpressionUUID->
  "e2f05fd2-7940-4f76-ac6e-bf26a63aa780"]
}], "Text",
 CellChangeTimes->{{3.795157190301571*^9, 3.795157260326276*^9}, {
   3.795157294244342*^9, 3.795157424123466*^9}, {3.795157463440308*^9, 
   3.7951575116914053`*^9}, {3.795157547986264*^9, 3.795157571953154*^9}, {
   3.795157606640985*^9, 3.795157610856587*^9}, {3.795157651992293*^9, 
   3.795157890550273*^9}, 3.7951579239822493`*^9, {3.79961065908285*^9, 
   3.799610770466672*^9}, {3.817101548697446*^9, 3.817101664141863*^9}, {
   3.817101730917473*^9, 3.817101749249284*^9}, {3.817101784302071*^9, 
   3.817101813051979*^9}, {3.817102575795018*^9, 
   3.817102583739436*^9}},ExpressionUUID->"1c24a554-9fa3-4be2-844a-\
5e6e34435d74"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Quantum", " ", "Hamiltonian"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"ham", "[", "f_", "]"}], " ", ":=", " ", 
     RowBox[{"Simplify", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         RowBox[{"D", "[", 
          RowBox[{"f", ",", 
           RowBox[{"{", 
            RowBox[{"x", ",", "2"}], "}"}]}], "]"}]}], "/", "2"}], " ", "+", 
       " ", 
       RowBox[{"f", " ", 
        RowBox[{
         RowBox[{"x", "^", "2"}], "/", "2"}]}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
    "we", " ", "test", " ", "by", " ", "operating", " ", "on", " ", 
     "exponetial", " ", "function"}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{"ham", "[", 
    RowBox[{"Exp", "[", "x", "]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7324333974957666`*^9, 3.7324333976676903`*^9}, {
  3.73243358735705*^9, 3.7324336043080225`*^9}, {3.7324336506974745`*^9, 
  3.732433699414362*^9}, {3.7324337458762465`*^9, 3.7324337690883017`*^9}, {
  3.7324339195256357`*^9, 3.7324339270140405`*^9}, {3.795157943998188*^9, 
  3.7951579765083103`*^9}, {3.799610774668352*^9, 3.7996107915538135`*^9}, {
  3.8171018829806557`*^9, 3.817101927735923*^9}},
 CellLabel->"In[63]:=",ExpressionUUID->"e0c68bc2-dcc1-4a50-8c67-ae28620c22c8"],

Cell[BoxData[
 TagBox[
  RowBox[{
   FractionBox["1", "2"], " ", 
   SuperscriptBox["\[ExponentialE]", "x"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     SuperscriptBox["x", "2"]}], ")"}]}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.732433770458237*^9, 3.7333791269369297`*^9, 3.7333792554347234`*^9, 
   3.733379447163316*^9, 3.7951579863690157`*^9, {3.7996107875624237`*^9, 
   3.7996107923870163`*^9}, 3.8171019439899273`*^9, 3.817107167926284*^9},
 CellLabel->"Out[64]=",ExpressionUUID->"d2fbfe42-efef-4601-8ea0-dc4f8cd60312"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Ground State ", "Section",
 CellChangeTimes->{{3.7324339346402855`*^9, 3.732433937850258*^9}, {
  3.81710209857098*^9, 
  3.817102118680031*^9}},ExpressionUUID->"6ec0a6e5-f5a5-429a-8cd8-\
346827ec754b"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"u", " ", "=", " ", 
    RowBox[{"Exp", "[", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"x", "^", "2"}]}], "/", "2"}], "]"}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"Ground", " ", "State", " ", "wave", " ", "function"}], " ", 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"xmax", " ", "=", " ", "5"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Plot", "[", 
  RowBox[{"u", ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{"-", "xmax"}], ",", "xmax"}], "}"}], ",", 
   RowBox[{"PlotRange", "\[Rule]", "All"}], ",", 
   RowBox[{"PlotLabel", " ", "\[Rule]", " ", "\"\<Ground State\>\""}]}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ham", "[", "u", "]"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"should", " ", 
    RowBox[{"be", ":", " ", 
     RowBox[{"energy", " ", "*", " ", "wave", " ", "function"}]}]}], " ", 
   "*)"}], " "}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<Ground State Energy= \>\"", ",", 
   RowBox[{
    RowBox[{"ham", "[", "u", "]"}], "/", "u"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7324339400378323`*^9, 3.7324339526857057`*^9}, {
  3.732433993213275*^9, 3.732434065975116*^9}, {3.7324340980228004`*^9, 
  3.7324340984159083`*^9}, {3.7324341498386126`*^9, 3.732434254521019*^9}, {
  3.795158057107561*^9, 3.795158084083352*^9}, {3.795158123962995*^9, 
  3.795158153458509*^9}, {3.795158194234579*^9, 3.795158197491211*^9}, {
  3.799610803892547*^9, 3.799610837897155*^9}, {3.799611028585931*^9, 
  3.7996110434085817`*^9}, {3.817102151866927*^9, 3.8171022326321793`*^9}, {
  3.817102640892714*^9, 3.817102726105698*^9}},
 CellLabel->"In[65]:=",ExpressionUUID->"f747eae2-ad0e-4da3-a906-48c5081dd2b8"]
}, Open  ]],

Cell[CellGroupData[{

Cell["n-th excited state", "Section",
 CellChangeTimes->{{3.732434290291602*^9, 3.7324342990631533`*^9}, {
  3.799611055605401*^9, 3.799611065720764*^9}, {3.817102523502286*^9, 
  3.817102561074594*^9}},ExpressionUUID->"d7314f66-5c06-497e-9b28-\
4a242a95663b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{
     "Harmonic", " ", "Oscillator", " ", "has", " ", "an", " ", "infinity", 
      " ", "of", " ", "solutions", " ", "numbered", " ", "n"}], "=", "0"}], 
    ",", "1", ",", "2", ",", 
    RowBox[{
     RowBox[{"...", "..."}], "..."}], ",", "\[Infinity]", ",", " ", 
    RowBox[{
     RowBox[{"n", "-", 
      RowBox[{"th", " ", "excited", " ", 
       RowBox[{"state", ":", " ", 
        RowBox[{"u", 
         RowBox[{"(", "x", ")"}]}]}]}]}], " ", "=", " ", 
     RowBox[{"n", "-", 
      RowBox[{
      "th", " ", "Hermite", " ", "polynomial", " ", "*", " ", "ground", " ", 
       "state", " ", "wave", " ", "function"}]}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"n", " ", "=", " ", "20"}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"n", "-", 
     RowBox[{"th", " ", "state"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{"u", " ", "=", " ", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{
       RowBox[{"-", 
        RowBox[{"x", "^", "2"}]}], "/", "2"}], "]"}], " ", 
     RowBox[{"HermiteH", "[", 
      RowBox[{"n", ",", "x"}], "]"}]}]}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"Wave", " ", "function", " ", 
     RowBox[{"(", 
      RowBox[{"not", " ", "normalised"}], ")"}]}], "  ", "*)"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.795158615859766*^9, 3.795158618308791*^9}, {
  3.795161631030797*^9, 3.795161644314502*^9}, {3.799611074717866*^9, 
  3.799611094902939*^9}, {3.8171029166422243`*^9, 3.8171029887490788`*^9}, {
  3.817103055213917*^9, 3.817103160476243*^9}, {3.817103256729911*^9, 
  3.817103363119069*^9}, {3.817103558562652*^9, 3.8171035593930492`*^9}},
 CellLabel->"In[70]:=",ExpressionUUID->"44be5399-13ea-45ea-bbb9-d6c1716bba29"],

Cell[TextData[{
 StyleBox["Check that function u fulfills Schr\[ODoubleDot]dinger eq.  and \
compute its energy: ener (should be: ",
  FontColor->RGBColor[1, 0, 0]],
 Cell[BoxData[
  FormBox[
   RowBox[{"n", "+", 
    FractionBox["1", "2"]}], TraditionalForm]],
  FontColor->RGBColor[1, 0, 0],ExpressionUUID->
  "ae39015a-6497-4eec-8f0b-8d39256878f5"],
 StyleBox[").",
  FontColor->RGBColor[1, 0, 0]]
}], "Text",
 CellChangeTimes->{{3.795158655059517*^9, 3.7951587620736933`*^9}, {
   3.7951588156811132`*^9, 3.7951591375431843`*^9}, 3.795159279424348*^9, {
   3.795159309574149*^9, 3.795159342076153*^9}, 3.799611112941968*^9, {
   3.7996114281285133`*^9, 3.799611475600254*^9}, {3.7996115829758115`*^9, 
   3.7996115874892607`*^9}, {3.799611623649593*^9, 3.799611624125269*^9}, {
   3.8171039979791937`*^9, 3.817104077729947*^9}, {3.817104116641532*^9, 
   3.817104127705398*^9}},ExpressionUUID->"25e24263-a9bc-4df5-80d0-\
e79c89435f67"],

Cell[BoxData[
 RowBox[{"ham", "[", "u", "]"}]], "Input",
 CellChangeTimes->{{3.799611483165593*^9, 3.799611496749769*^9}},
 CellLabel->"In[72]:=",ExpressionUUID->"a407e51d-78ba-4558-971a-9880fb4964e5"],

Cell[BoxData[
 RowBox[{"ener", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"ham", "[", "u", "]"}], "/", "u"}], "//", "Simplify"}]}]], "Input",\

 CellChangeTimes->{{3.799611538882039*^9, 3.79961154813636*^9}},
 CellLabel->"In[73]:=",ExpressionUUID->"20490668-006e-45ef-be95-10ba07000a70"],

Cell[TextData[{
 StyleBox["Answer: u fulfills Schr\[ODoubleDot]dingereq. with:  ",
  FontWeight->"Bold"],
 Cell[BoxData[
  FormBox[
   RowBox[{"ener", "=", 
    RowBox[{
     RowBox[{"20", "+", 
      FractionBox["1", "2"]}], "=", 
     FractionBox["41", "2"]}]}], TraditionalForm]],
  FontWeight->"Bold",ExpressionUUID->"0d0ca5c1-ee7b-4092-b334-20e6c3fcbb4c"],
 "."
}], "Text",
 CellChangeTimes->{{3.795158655059517*^9, 3.7951587620736933`*^9}, {
   3.7951588156811132`*^9, 3.7951591375431843`*^9}, 3.795159279424348*^9, {
   3.795159309574149*^9, 3.795159342076153*^9}, 3.799611112941968*^9, {
   3.7996114281285133`*^9, 3.7996114442971735`*^9}, {3.799611614317857*^9, 
   3.799611682549741*^9}, {3.817104214025866*^9, 
   3.817104254752212*^9}},ExpressionUUID->"93a31352-09f0-433a-b797-\
dc8aa283e6e5"],

Cell[TextData[StyleBox["Plot u (choose the correct range xmax and do not \
forget to label the plot! )",
 FontColor->RGBColor[1, 0, 0]]], "Text",
 CellChangeTimes->{{3.795158655059517*^9, 3.7951587620736933`*^9}, {
   3.7951588156811132`*^9, 3.7951591375431843`*^9}, 3.795159279424348*^9, {
   3.795159309574149*^9, 3.795159342076153*^9}, 3.799611112941968*^9, {
   3.7996114281285133`*^9, 3.7996114442971735`*^9}, 3.7996115115952516`*^9, 
   3.7996117067998314`*^9, {3.8171042779836683`*^9, 3.8171043096314363`*^9}, {
   3.817104348934784*^9, 
   3.81710437984706*^9}},ExpressionUUID->"857b142e-5e67-40e8-8332-\
8be3cd055a33"],

Cell[BoxData[
 RowBox[{
  RowBox[{"xmax", "=", "10"}], ";", 
  RowBox[{"Plot", "[", 
   RowBox[{"u", ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"-", "xmax"}], ",", "xmax"}], "}"}], ",", 
    RowBox[{"PlotRange", "\[Rule]", "All"}], ",", 
    RowBox[{
    "PlotLabel", " ", "\[Rule]", " ", "\"\<20-ty stan wzbudzony\>\""}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.799611708694078*^9, 3.7996117740468116`*^9}},
 CellLabel->"In[74]:=",ExpressionUUID->"938886bd-0a92-435c-ab72-487f5731dc2b"],

Cell[TextData[{
 StyleBox["Normalise u: \nprobability density ro=Abs[u/",
  FontColor->RGBColor[1, 0, 0]],
 Cell[BoxData[
  FormBox[
   SqrtBox["norm"], TraditionalForm]],
  FontColor->RGBColor[1, 0, 0],ExpressionUUID->
  "d9bf0d2d-13f7-4191-b9cb-81f528a16736"],
 StyleBox["]^2 \nwhere norm = ",
  FontColor->RGBColor[1, 0, 0]],
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], " ", 
    RowBox[{
     RowBox[{"u", "^", "2"}], "  ", 
     RowBox[{"\[DifferentialD]", "x"}]}]}], TraditionalForm]],
  FontColor->RGBColor[1, 0, 0],ExpressionUUID->
  "5840081b-7cd4-4915-8196-2f2fabaa5bfc"],
 StyleBox["\nshould fulfill ",
  FontColor->RGBColor[1, 0, 0]],
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubsuperscriptBox["\[Integral]", 
     RowBox[{"-", "\[Infinity]"}], "\[Infinity]"], 
    RowBox[{"ro", 
     RowBox[{"\[DifferentialD]", "x"}]}]}], TraditionalForm]],
  FormatType->"TraditionalForm",
  FontColor->RGBColor[1, 0, 0],ExpressionUUID->
  "779eb0bf-4822-46d6-9482-0a5fb1ccc023"],
 StyleBox[" = 1",
  FontColor->RGBColor[1, 0, 0]]
}], "Text",
 CellChangeTimes->{{3.795158655059517*^9, 3.7951587620736933`*^9}, {
   3.7951588156811132`*^9, 3.7951591375431843`*^9}, 3.795159279424348*^9, {
   3.795159309574149*^9, 3.795159342076153*^9}, 3.799611112941968*^9, {
   3.7996114281285133`*^9, 3.7996114442971735`*^9}, 3.7996115115952516`*^9, {
   3.799611790455964*^9, 3.7996117912573214`*^9}, {3.7996118543624287`*^9, 
   3.799611855786244*^9}, {3.8171045694791107`*^9, 3.817104675375525*^9}, {
   3.817104706418971*^9, 3.817104706419118*^9}, {3.8171047955030603`*^9, 
   3.817104840658337*^9}, {3.817105957585599*^9, 
   3.817105962503584*^9}},ExpressionUUID->"b5459232-c783-458c-9c37-\
7b2fa55e4c21"],

Cell[BoxData[
 RowBox[{"norm", "=", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    SuperscriptBox["u", "2"], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"-", "\[Infinity]"}], ",", "\[Infinity]"}], "}"}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.7996118680148525`*^9, 3.7996118867568445`*^9}},
 CellLabel->"In[75]:=",ExpressionUUID->"d84f109c-0284-4075-95d1-c667244bab08"],

Cell[BoxData[
 RowBox[{
  RowBox[{"xmax", "=", "10"}], ";", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"u", "/", 
     SqrtBox["norm"]}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"-", "xmax"}], ",", "xmax"}], "}"}], ",", 
    RowBox[{"PlotRange", "\[Rule]", "All"}], ",", 
    RowBox[{
    "PlotLabel", " ", "\[Rule]", " ", 
     "\"\<Unormowany 20-ty stan wzbudzony\>\""}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7996119074809675`*^9, 3.7996119281769238`*^9}},
 CellLabel->"In[76]:=",ExpressionUUID->"52d25b70-e78c-4fa4-b2ae-8cdddbdd68a1"],

Cell[BoxData[
 RowBox[{"\[Rho]", "=", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"Abs", "[", 
     FractionBox["u", 
      SqrtBox["norm"]], "]"}], "2"], "//", "Simplify"}]}]], "Input",
 CellChangeTimes->{{3.7996119358535547`*^9, 3.799611956384408*^9}},
 CellLabel->"In[77]:=",ExpressionUUID->"964c4026-6465-4faf-a13e-cc3cab0eb046"],

Cell[TextData[StyleBox["Plot plqm = plot of normalised probability density \
(choose xmax)",
 FontColor->RGBColor[1, 0, 0]]], "Text",
 CellChangeTimes->{{3.795158655059517*^9, 3.7951587620736933`*^9}, {
   3.7951588156811132`*^9, 3.7951591375431843`*^9}, 3.795159279424348*^9, {
   3.795159309574149*^9, 3.795159342076153*^9}, 3.799611112941968*^9, {
   3.7996114281285133`*^9, 3.7996114442971735`*^9}, 3.7996115115952516`*^9, {
   3.799611790455964*^9, 3.7996117912573214`*^9}, {3.7996119613040814`*^9, 
   3.7996119617766886`*^9}, {3.7996119990470295`*^9, 
   3.7996120056473165`*^9}, {3.817104957686132*^9, 3.81710502761073*^9}, {
   3.817105977430648*^9, 
   3.817105981998329*^9}},ExpressionUUID->"41070e20-c400-401a-b4f6-\
c0a340650767"],

Cell[BoxData[
 RowBox[{
  RowBox[{"xmax", "=", "10"}], ";", 
  RowBox[{"plqm", "=", 
   RowBox[{"Plot", "[", 
    RowBox[{"\[Rho]", ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"-", "xmax"}], ",", "xmax"}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", "All"}], ",", 
     RowBox[{
     "PlotLabel", " ", "\[Rule]", " ", 
      "\"\<Kwantowy rozk\[LSlash]ad prawdopodobie\:0144stwa\>\""}]}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.799611965274129*^9, 3.7996119896376667`*^9}, {
  3.7996120208669367`*^9, 3.799612033722431*^9}, {3.7996125529865007`*^9, 
  3.7996125544490557`*^9}},
 CellLabel->"In[78]:=",ExpressionUUID->"3d0ec155-cce9-4c6b-bd13-b8885d1d2d67"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Classical particle", "Section",
 CellChangeTimes->{{3.732435373352765*^9, 3.732435379669866*^9}, {
  3.7324356259585896`*^9, 3.732435626224231*^9}, {3.817105343543949*^9, 
  3.8171053570462713`*^9}},ExpressionUUID->"e4ce8d96-701c-4b80-aaf2-\
3a9a3b6cef07"],

Cell[TextData[{
 "Classical hamiltonian\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["H", "classical"], "=", 
    RowBox[{
     FractionBox[
      SuperscriptBox["p", "2"], "2"], "+", 
     FractionBox[
      SuperscriptBox["x", "2"], "2"]}]}], TraditionalForm]],ExpressionUUID->
  "0c79b0fe-96c5-45d1-8212-42c5dd6816d2"],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"p", 
     RowBox[{"(", "x", ")"}]}], "=", 
    RowBox[{
     RowBox[{"v", "(", "x", ")"}], "=", 
     FractionBox[
      RowBox[{"dx", "(", "t", ")"}], "dt"]}]}], TraditionalForm]],
  ExpressionUUID->"e16c4bbd-9688-433e-8f6c-46e3fec88b73"],
 "\n\nClassical particle with energy ener can be found in region [x,x+dx] \
with probability proportional to the time dt\nit spends in region [x,x+dx]\n\
We have (\[Rho] : probability distribution):\n\[Rho](x) dx \[Tilde] dt,   \
that is: \[Rho](x) \[Tilde] ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    FractionBox["dt", "dx"], "=", 
    FractionBox["1", 
     RowBox[{"v", "(", "x", ")"}]]}], TraditionalForm]],ExpressionUUID->
  "a83ea7f9-2f04-42f7-a277-5b6102f24673"],
 "\nIn our case (particle moves between  -xmaxcl < x < xmaxcl)\n",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"v", "(", "x", ")"}], "2"], TraditionalForm]],ExpressionUUID->
  "60d3637b-852f-4d28-9abf-6de4b83ce13b"],
 " = ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"p", "(", "x", ")"}], "2"], TraditionalForm]],ExpressionUUID->
  "e63a63fc-d96a-4365-a728-901954c3113f"],
 " = 2 ener - ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["x", "2"], TraditionalForm]],ExpressionUUID->
  "f55d6cca-7a90-4fd2-a1a6-c95d9b3afc3f"],
 " > 0   - xmaxcl < x < xmaxcl\nxmaxcl=",
 Cell[BoxData[
  FormBox[
   SqrtBox[
    RowBox[{"2", " ", "ener"}]], TraditionalForm]],ExpressionUUID->
  "4d6c7b0e-5518-4abf-810f-93fc0e302632"],
 "\n\n\n\n",
 StyleBox["Normalise the classical probability density.\nplot it with \
PlotStyle->Red, PlotLabel->\[CloseCurlyDoubleQuote]Classical probability \
density.\nPlot should be named plcl\n\n\n\nadd classical limits for particle \
with energy ener:",
  FontColor->RGBColor[1, 0, 0]]
}], "Text",
 CellChangeTimes->{
  3.7951593812620077`*^9, {3.795159685943884*^9, 3.7951599547952433`*^9}, {
   3.795159988435734*^9, 3.795160005858623*^9}, {3.79516006036189*^9, 
   3.7951600921867933`*^9}, {3.795160175550942*^9, 3.795160230634755*^9}, {
   3.795160261034891*^9, 3.795160269940782*^9}, {3.795160338355414*^9, 
   3.795160389188232*^9}, {3.7951604451146317`*^9, 3.795160480626513*^9}, {
   3.795160562544485*^9, 3.795160662535391*^9}, {3.7951607014737673`*^9, 
   3.795160774246368*^9}, {3.79516081168573*^9, 3.795160871241284*^9}, {
   3.795160944940268*^9, 3.795160993347066*^9}, {3.795161083289765*^9, 
   3.795161150057928*^9}, {3.795161192288267*^9, 3.7951612439449472`*^9}, {
   3.795161279239326*^9, 3.795161347823162*^9}, {3.795161390607259*^9, 
   3.795161412719491*^9}, {3.795161743988544*^9, 3.795161755177444*^9}, {
   3.7952345322111073`*^9, 3.795234537394006*^9}, {3.7996122344003496`*^9, 
   3.799612305135066*^9}, {3.817105362422764*^9, 3.817105415383691*^9}, {
   3.817105452781796*^9, 3.817105598599718*^9}, {3.817105642020149*^9, 
   3.817105673291954*^9}, 3.817105711194282*^9, {3.817105743603636*^9, 
   3.817105781585505*^9}, {3.817105851348002*^9, 3.817105906704414*^9}, {
   3.817106057286902*^9, 3.8171063278660603`*^9}, {3.827728950163282*^9, 
   3.827728969543359*^9}},ExpressionUUID->"42600042-94fa-4455-a92c-\
ec68cd725d67"],

Cell[BoxData[
 RowBox[{"xmaxcl", "=", 
  SqrtBox[
   RowBox[{"2", " ", "ener"}]]}]], "Input",
 CellChangeTimes->{{3.7996123669567375`*^9, 3.799612375389399*^9}},
 CellLabel->"In[79]:=",ExpressionUUID->"b0124d09-579a-4b4e-acc5-2bc8a566ac3e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"v", "[", "x_", "]"}], ":=", 
  SqrtBox[
   RowBox[{
    RowBox[{"2", " ", "ener"}], "-", 
    SuperscriptBox["x", "2"]}]]}]], "Input",
 CellChangeTimes->{{3.79961238403755*^9, 3.799612411858569*^9}},
 CellLabel->"In[80]:=",ExpressionUUID->"8254cf89-4547-4e75-ac45-75dae17eac8c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Rho]cl", "[", "x_", "]"}], ":=", 
  RowBox[{"1", "/", 
   RowBox[{"v", "[", "x", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.799612429943904*^9, 3.7996124369805083`*^9}},
 CellLabel->"In[81]:=",ExpressionUUID->"d3b4e9b4-94b5-4499-aed1-84c2ddf2764e"],

Cell[BoxData[
 RowBox[{"normcl", "=", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"\[Rho]cl", "[", "x", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"-", "xmaxcl"}], ",", "xmaxcl"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7996124409207773`*^9, 3.799612482144661*^9}},
 CellLabel->"In[82]:=",ExpressionUUID->"44e08550-da85-4c7b-a62e-7f6f2cccfc37"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Rho]clu", "[", "x_", "]"}], ":=", 
  RowBox[{
   RowBox[{"\[Rho]cl", "[", "x", "]"}], "/", "normcl"}]}]], "Input",
 CellChangeTimes->{{3.7996124877259912`*^9, 3.7996125008889027`*^9}},
 CellLabel->"In[83]:=",ExpressionUUID->"aba541c8-de5c-4310-a1e7-9aa2bfeaa0e0"],

Cell[BoxData[
 RowBox[{"plcl1", "=", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"\[Rho]clu", "[", "x", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"-", "xmaxcl"}], ",", "xmaxcl"}], "}"}], ",", 
    RowBox[{"PlotRange", "\[Rule]", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0.25"}], "}"}]}], ",", 
    RowBox[{
    "PlotLabel", " ", "\[Rule]", " ", 
     "\"\<Klasyczny rozk\[LSlash]ad prawdopodobie\:0144stwa\>\""}], ",", 
    RowBox[{"PlotStyle", "\[Rule]", "Red"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7996125282280483`*^9, 3.7996126084632006`*^9}, {
  3.799612662217012*^9, 3.799612664746621*^9}, {3.799612697808607*^9, 
  3.799612764762411*^9}},
 CellLabel->"In[84]:=",ExpressionUUID->"178ba291-6990-4791-9c52-d71ac8b42d34"],

Cell[BoxData[
 RowBox[{
  RowBox[{"plcl", "=", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"\[Rho]clu", "[", "x", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"-", "xmaxcl"}], ",", "xmaxcl"}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0.25"}], "}"}]}], ",", 
     RowBox[{"PlotStyle", "\[Rule]", "Red"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.799612746306687*^9, 3.7996127709537525`*^9}},
 CellLabel->"In[85]:=",ExpressionUUID->"a01dd75e-7ecc-4a5a-a3eb-8ee7b9a5f387"],

Cell["\<\

Adding the limits of classical movement (dashed lines)\
\>", "Text",
 CellChangeTimes->{
  3.7951593812620077`*^9, {3.795159685943884*^9, 3.7951599547952433`*^9}, {
   3.795159988435734*^9, 3.795160005858623*^9}, {3.79516006036189*^9, 
   3.7951600921867933`*^9}, {3.795160175550942*^9, 3.795160230634755*^9}, {
   3.795160261034891*^9, 3.795160269940782*^9}, {3.795160338355414*^9, 
   3.795160389188232*^9}, {3.7951604451146317`*^9, 3.795160480626513*^9}, {
   3.795160562544485*^9, 3.795160662535391*^9}, {3.7951607014737673`*^9, 
   3.795160774246368*^9}, {3.79516081168573*^9, 3.795160871241284*^9}, {
   3.795160944940268*^9, 3.795160993347066*^9}, {3.795161083289765*^9, 
   3.795161150057928*^9}, {3.795161192288267*^9, 3.7951612439449472`*^9}, {
   3.795161279239326*^9, 3.795161347823162*^9}, {3.795161390607259*^9, 
   3.795161412719491*^9}, {3.795161743988544*^9, 3.795161755177444*^9}, {
   3.7952345322111073`*^9, 3.795234537394006*^9}, {3.7996122344003496`*^9, 
   3.799612305135066*^9}, {3.799612616000323*^9, 3.7996126187364655`*^9}, {
   3.8171065447518053`*^9, 
   3.817106596789009*^9}},ExpressionUUID->"e6e76c41-b62e-42bb-b102-\
49badd448427"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"klasyczna", "=", 
   RowBox[{"Graphics", "[", 
    RowBox[{"{", 
     RowBox[{
     "Dashed", ",", "Blue", ",", "Thick", ",", "\[IndentingNewLine]", 
      RowBox[{"Line", "[", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"xmaxcl", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"xmaxcl", ",", 
           RowBox[{"1", "/", "4"}]}], "}"}]}], "}"}], "]"}]}], "}"}], "]"}]}],
   ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"klasyczna1", "=", 
    RowBox[{"Graphics", "[", 
     RowBox[{"{", 
      RowBox[{"Dashed", ",", "Blue", ",", "Thick", ",", "\[IndentingNewLine]", 
       RowBox[{"Line", "[", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{
            RowBox[{"-", "xmaxcl"}], ",", "0"}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"-", "xmaxcl"}], ",", 
            RowBox[{"1", "/", "4"}]}], "}"}]}], "}"}], "]"}]}], "}"}], 
     "]"}]}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  "\"\<\n\n\n\t\t\tG\:0119sto\:015bci prawdopodobie\:0144stwa\n               \
  klasyczna(czerwona)\n                 kwantowa(niebieski)\nPionowe linie \
przerywane pokazuj\:0105 granic\:0119 obszaru klasycznie dozwolonego\>\"", 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
  "plcl", ",", " ", "plqm", ",", " ", "klasyczna", ",", " ", "klasyczna1"}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.795161491701296*^9, 3.795161492629755*^9}, {
   3.7996128052148094`*^9, 3.799612880362399*^9}, {3.817106736932667*^9, 
   3.817106760412416*^9}, {3.817106864049981*^9, 3.817106922544815*^9}, 
   3.817107143352759*^9},
 CellLabel->"In[86]:=",ExpressionUUID->"5fe40036-5659-41c2-b110-2e07a7abd716"],

Cell[TextData[{
 StyleBox["To do:",
  FontSize->24,
  FontColor->RGBColor[1, 0, 0]],
 StyleBox["\n run the notebook several times with n= 0, 5 10, 20.\nAt each \
run save the final figure as fig0.pdf , fig5.pdf   ... etc.\nTo save mark the \
square bracket at the right and use File->Save selection as\nSend me the \
graphs only ",
  FontColor->RGBColor[1, 0, 0]],
 "\n"
}], "Text",
 CellChangeTimes->{
  3.827729340350618*^9, {3.827729399683333*^9, 3.827729583948395*^9}, {
   3.827729642654477*^9, 3.82772979461122*^9}, {3.827729872347301*^9, 
   3.827729900480935*^9}},ExpressionUUID->"5f07679b-54e7-449a-9f8a-\
87cd29e4a490"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1314, 795},
WindowMargins->{{6, Automatic}, {-43, Automatic}},
Magnification->1.5,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 270, 4, 146, "Title",ExpressionUUID->"17a1f910-0a7c-4b08-9383-7f40d283aabf"],
Cell[CellGroupData[{
Cell[875, 30, 465, 7, 78, "Input",ExpressionUUID->"4ef42661-f4d0-4d12-81d6-7a0094905056"],
Cell[1343, 39, 408, 8, 52, "Output",ExpressionUUID->"c3e2c797-92bf-4df8-a9c1-84e62df0626e"]
}, Open  ]],
Cell[1766, 50, 1521, 39, 183, "Text",ExpressionUUID->"1c24a554-9fa3-4be2-844a-5e6e34435d74"],
Cell[CellGroupData[{
Cell[3312, 93, 1401, 34, 171, "Input",ExpressionUUID->"e0c68bc2-dcc1-4a50-8c67-ae28620c22c8"],
Cell[4716, 129, 603, 15, 68, "Output",ExpressionUUID->"d2fbfe42-efef-4601-8ea0-dc4f8cd60312"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5356, 149, 208, 4, 101, "Section",ExpressionUUID->"6ec0a6e5-f5a5-429a-8cd8-346827ec754b"],
Cell[5567, 155, 1742, 40, 171, "Input",ExpressionUUID->"f747eae2-ad0e-4da3-a906-48c5081dd2b8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7346, 200, 260, 4, 101, "Section",ExpressionUUID->"d7314f66-5c06-497e-9b28-4a242a95663b"],
Cell[7609, 206, 1827, 46, 171, "Input",ExpressionUUID->"44be5399-13ea-45ea-bbb9-d6c1716bba29"],
Cell[9439, 254, 939, 20, 59, "Text",ExpressionUUID->"25e24263-a9bc-4df5-80d0-e79c89435f67"],
Cell[10381, 276, 201, 3, 46, "Input",ExpressionUUID->"a407e51d-78ba-4558-971a-9880fb4964e5"],
Cell[10585, 281, 285, 7, 46, "Input",ExpressionUUID->"20490668-006e-45ef-be95-10ba07000a70"],
Cell[10873, 290, 805, 19, 61, "Text",ExpressionUUID->"93a31352-09f0-433a-b797-dc8aa283e6e5"],
Cell[11681, 311, 627, 10, 53, "Text",ExpressionUUID->"857b142e-5e67-40e8-8332-8be3cd055a33"],
Cell[12311, 323, 513, 13, 78, "Input",ExpressionUUID->"938886bd-0a92-435c-ab72-487f5731dc2b"],
Cell[12827, 338, 1780, 44, 168, "Text",ExpressionUUID->"b5459232-c783-458c-9c37-7b2fa55e4c21"],
Cell[14610, 384, 394, 10, 49, "Input",ExpressionUUID->"d84f109c-0284-4075-95d1-c667244bab08"],
Cell[15007, 396, 571, 15, 88, "Input",ExpressionUUID->"52d25b70-e78c-4fa4-b2ae-8cdddbdd68a1"],
Cell[15581, 413, 332, 8, 74, "Input",ExpressionUUID->"964c4026-6465-4faf-a13e-cc3cab0eb046"],
Cell[15916, 423, 743, 12, 53, "Text",ExpressionUUID->"41070e20-c400-401a-b4f6-c0a340650767"],
Cell[16662, 437, 692, 17, 78, "Input",ExpressionUUID->"3d0ec155-cce9-4c6b-bd13-b8885d1d2d67"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17391, 459, 262, 4, 101, "Section",ExpressionUUID->"e4ce8d96-701c-4b80-aaf2-3a9a3b6cef07"],
Cell[17656, 465, 3510, 83, 765, "Text",ExpressionUUID->"42600042-94fa-4455-a92c-ec68cd725d67"],
Cell[21169, 550, 240, 5, 49, "Input",ExpressionUUID->"b0124d09-579a-4b4e-acc5-2bc8a566ac3e"],
Cell[21412, 557, 313, 8, 57, "Input",ExpressionUUID->"8254cf89-4547-4e75-ac45-75dae17eac8c"],
Cell[21728, 567, 285, 6, 46, "Input",ExpressionUUID->"d3b4e9b4-94b5-4499-aed1-84c2ddf2764e"],
Cell[22016, 575, 391, 9, 46, "Input",ExpressionUUID->"44e08550-da85-4c7b-a62e-7f6f2cccfc37"],
Cell[22410, 586, 300, 6, 46, "Input",ExpressionUUID->"aba541c8-de5c-4310-a1e7-9aa2bfeaa0e0"],
Cell[22713, 594, 768, 18, 78, "Input",ExpressionUUID->"178ba291-6990-4791-9c52-d71ac8b42d34"],
Cell[23484, 614, 569, 14, 46, "Input",ExpressionUUID->"a01dd75e-7ecc-4a5a-a3eb-8ee7b9a5f387"],
Cell[24056, 630, 1175, 20, 88, "Text",ExpressionUUID->"e6e76c41-b62e-42bb-b102-49badd448427"],
Cell[25234, 652, 1872, 47, 481, "Input",ExpressionUUID->"5fe40036-5659-41c2-b110-2e07a7abd716"],
Cell[27109, 701, 629, 15, 242, "Text",ExpressionUUID->"5f07679b-54e7-449a-9f8a-87cd29e4a490"]
}, Open  ]]
}, Open  ]]
}
]
*)

